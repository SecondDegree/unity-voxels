﻿using System.Collections.Generic;
using UnityEngine;

public enum CubeFace
{
    Front = 0,  // Z - Positive
    Back,       // Z - Negative
    Right,      // X - Positive
    Left,       // X - Negative
    Top,        // Y - Positive
    Bottom      // Y - Negative
}

public struct Quad
{
    public Vertex[] Vertices;
    public int[] Indices;
}

public struct Vertex
{
    public Vector3 Position;
    public Vector3 Normal;
    public Vector2 Uvs;
}

public static class ProceduralMeshHelper
{
    public static Quad MakeCubeFace(int face, float scale, Vector3 offset)
    {
        face %= 6; // Make sure face is 0-5

        Quad newQuad = new Quad { Vertices = new Vertex[4] };

        for(int i = 0; i < 4; ++i)
        {
            newQuad.Vertices[i].Position = MeshData.CubePoints[MeshData.CubeQuadPoints[face][i]] * scale + offset;
            newQuad.Vertices[i].Normal = MeshData.CubeNormals[face];
            newQuad.Vertices[i].Uvs = MeshData.QuadUVs[MeshData.CubeQuadUVs[face][i]];
        }

        // Right, Left
        int winding = 0;

        // Pick winding order by face.
        switch(face)
        {
            // Front, Back, Top, Bottom
            case 0: case 1: case 4: case 5: 
                winding = 1;
                break;
        }

        newQuad.Indices = MeshData.QuadIndices[winding];

        return newQuad;
    }

    public static Quad MakeCubeFace(CubeFace face, float scale, Vector3 offset)
    {
        return MakeCubeFace((int)face, scale, offset);
    }
}
