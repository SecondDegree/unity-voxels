﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using UnityEngine.Serialization;
using Debug = System.Diagnostics.Debug;
using Random = UnityEngine.Random;

public enum VoxelType
{
    None, // Empty space
    Dirt,
    Stone,
    Water
}

[System.Serializable]
public struct Voxel
{
    public VoxelType type;
}

public class VoxelController : MonoBehaviour
{
    private Voxel[] _voxels = { new Voxel { type = VoxelType.Dirt } };

    public ref readonly Voxel[] Voxels => ref _voxels;

    // The size (in metres) of a single voxel in each dimension
    public static readonly float VoxelSize = 1f;

    [Range(1, 40)]
    public int worldXSize = 5;
    [Range(1, 256)]
    public int worldYSize = 5;
    [Range(1, 40)]
    public int worldZSize = 5;

    // Editor only stuff
    int _prevWorldXSize, _prevWorldYSize, _prevWorldZSize;

    public bool drawDebugVoxelPoints;
    public bool regenerate;

    public bool toggle;
    [FormerlySerializedAs("ToggleSpot")] public Vector3Int toggleSpot;

    [ReadOnlyInspector]
    public int numVoxels;

    public delegate void OnVoxelsChanged();
    public static event OnVoxelsChanged OnVoxelsChangedEvent;

    private List<int> _dirtyVoxels = new List<int>();

    // Run the code on editor changed so we can see it without playing
    private void OnValidate()
    {
        if(regenerate || _prevWorldXSize != worldXSize || _prevWorldYSize != worldYSize || _prevWorldZSize != worldZSize)
        {
            GenerateVoxelWorldX(worldXSize, worldYSize, worldZSize);
            gameObject.GetComponent<VoxelMesh>().ForceRegenMesh();
            _prevWorldXSize = worldXSize;
            _prevWorldYSize = worldYSize;
            _prevWorldZSize = worldZSize;
            regenerate = false;
        }

        if (!toggle) return;

        var index = GetVoxelIndex(toggleSpot);

        if(IsVoxelIndexValid(index))
        {
            if(IsVoxelEmpty(index))
            {
                AddVoxel(index, VoxelType.Dirt);
            }
            else
            {
                RemoveVoxel(index);
            }
        }

        gameObject.GetComponent<VoxelMesh>().ForceRegenMesh();

        toggle = false;
    }

    private void OnDrawGizmosSelected()
    {
        if(drawDebugVoxelPoints == false || _voxels.Length > 10000) return;

        Gizmos.color = Color.yellow;

        for(int i = 0; i < _voxels.Length; ++i)
        {
            Gizmos.DrawSphere((Vector3)GetVoxelRelativePosition(i) + transform.position, 0.05f);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(_dirtyVoxels.Count > 0)
        {
            OnVoxelsChangedEvent?.Invoke();
            _dirtyVoxels.Clear();
        }
    }

    public ref readonly Voxel[] GetVoxels() => ref _voxels;

    public bool IsVoxelNotEmpty(int? index) => !IsVoxelEmpty(index);

    public bool IsVoxelEmpty(int? index)
    {
        if(IsVoxelIndexValid(index) == false) return false;

        return _voxels[index.Value].type == VoxelType.None;
    }

    // Voxel Manipulation
    public void RemoveVoxel(int? index) => SetVoxelType(index, VoxelType.None);

    public void AddVoxel(int? index, VoxelType newType)
    {
        if(newType == VoxelType.None) return; // Don't allow this to remove voxels
        SetVoxelType(index, newType);
    }

    public void SetVoxelType(int? index, VoxelType newType)
    {
        if(IsVoxelIndexValid(index) == false) return;
        Debug.Assert(index != null, nameof(index) + " != null");
        int i = index.Value;

        if(_voxels[i].type == newType) return; // Voxel is already this type

        _voxels[i].type = newType;
        MarkVoxelDirty(i);
    }

    private void MarkVoxelDirty(int index) => _dirtyVoxels.Add(index);

    private bool IsVoxelIndexValid(int? index) => index != null && IsVoxelIndexValid(index.Value);
    private bool IsVoxelIndexValid(int index) => index >= 0 && index < _voxels.Length;

    /// <summary>
    /// Creates a fresh, new voxel of the given size in each dimension (will be a cube)
    /// Centred around the origin of this GameObject
    /// </summary>
    /// <param name="xScale">Number of voxels wide</param>
    /// <param name="yScale">Number of voxels high</param>
    /// <param name="zScale">Number of voxels deep</param>
    void GenerateVoxelWorldX(int xScale = 1, int yScale = 1, int zScale = 1)
    {
        numVoxels = xScale * yScale * zScale;

        _dirtyVoxels.Clear();
        _voxels = new Voxel[numVoxels];

        // Generate size^3 voxels
        for(int i = 0; i < numVoxels; ++i)
        {
            //var pos = GetVoxel3DIndex_Internal(i);
            _voxels[i] = new Voxel { type = Random.value < 0.7f ? VoxelType.Dirt : VoxelType.None };
        }

        OnVoxelsChangedEvent?.Invoke();
    }

    /// <summary>
    /// Attempts to convert the given 3D index into the 1D equivelent
    /// </summary>
    /// <returns></returns>
    public int? GetVoxelIndex(int x, int y, int z)
    {
        // Early escape if any single axis is out of bounds. Prevents wrapping.
        if(x >= worldXSize || x < 0 || y >= worldYSize || y < 0 || z >= worldZSize || z < 0) return null;
        var index = x + y * worldXSize + z * worldXSize * worldYSize;
        if(IsVoxelIndexValid(index) == false) return null;
        return index;
    }

    public int? GetVoxelIndex(Vector3Int gridPosition) => GetVoxelIndex(gridPosition.x, gridPosition.y, gridPosition.z);
    public int? GetVoxelIndex(int3 gridPosition) => GetVoxelIndex(gridPosition.x, gridPosition.y, gridPosition.z);

    /// <summary>
    /// Converts a 1D voxel index into a 3D voxel index. Useful for spatialising voxels
    /// </summary>
    public int3? GetVoxel3DIndex(int? index)
    {
        if(IsVoxelIndexValid(index) == false) return null;
        return GetVoxel3DIndex(index.Value);
    }

    /// <summary>
    /// Converts a 1D voxel index into a 3D voxel index. Useful for spatialising voxels
    /// </summary>
    public int3 GetVoxel3DIndex(int index) => new int3(index % worldXSize, // Wraps back around when it equals 'x size'
                                                        index / worldXSize % worldYSize, // Increases every time 'index' increases by 'x size', wrapping back around when it equals 'y size'
                                                        index / (worldXSize* worldYSize)); // Increases every time 'index' increases by 'x size' * 'y size'

    /// <summary>
    /// Returns the location in 3D space of the given voxel relative to this game object
    /// </summary>
    public float3? GetVoxelRelativePosition(int? index)
    {
        if(IsVoxelIndexValid(index) == false) return null;
        Debug.Assert(index != null, nameof(index) + " != null");
        var i = index.Value;

        var vec = new float3(GetVoxel3DIndex(i));
        vec -= GetVoxelOffset(i);
        return vec;
    }

    public float3 GetVoxelOffset(int index) => new float3
    { x = VoxelSize * (worldXSize - 1) / 2f,
                                                               y = VoxelSize * (worldYSize - 1) / 2f,
                                                               z = VoxelSize * (worldZSize - 1) / 2f };

    /// <summary>
    /// Attempts to get the voxel next to the given voxel in the given direction.
    /// </summary>
    /// <returns>The adjacent voxel's index or null if none exists or given index is invalid</returns>
    public int? GetAdjacentVoxel(int? index, CubeFace direction)
    {
        if(IsVoxelIndexValid(index) == false) return null;
        Debug.Assert(index != null, nameof(index) + " != null");

        var direction3D = new int3();

        switch(direction)
        {
            case CubeFace.Front:
                direction3D.z = 1;
                break;
            case CubeFace.Back:
                direction3D.z = -1;
                break;
            case CubeFace.Right:
                direction3D.x = 1;
                break;
            case CubeFace.Left:
                direction3D.x = -1;
                break;
            case CubeFace.Top:
                direction3D.y = 1;
                break;
            case CubeFace.Bottom:
                direction3D.y = -1;
                break;
            default:
                // Fail if direction is invalid
                return null;
        }

        var index3D = GetVoxel3DIndex(index.Value);
        index3D += direction3D;
        return GetVoxelIndex(index3D);
    }
    public int? GetAdjacentVoxel(int? voxelIndex, byte direction) => GetAdjacentVoxel(voxelIndex, (CubeFace)direction);
}
