﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.Mathematics;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(VoxelController))]
public class VoxelMesh : MonoBehaviour
{
    [SerializeField, HideInInspector]
    private MeshFilter meshFilter;
    [SerializeField, HideInInspector]
    private Mesh procMesh;

    [SerializeField, HideInInspector]
    private VoxelController voxelController;

    [Range(0,1)]
    public int jiggle;

    [ReadOnlyInspector]
    public int numVertices;
    [ReadOnlyInspector]
    public int numIndices;

    public bool drawDebugPoints;
    public bool drawDebugNormals;

    private void OnValidate()
    {
        if(voxelController == null) voxelController = GetComponent<VoxelController>();
        if(meshFilter == null) meshFilter = GetComponent<MeshFilter>();

        // TODO: This causes duplicated gameobjects to share a mesh...
        if(procMesh == null)
        {
            procMesh = new Mesh();
            meshFilter.mesh = procMesh;
            procMesh.name = "Voxel Mesh";
        }

        RegenerateMesh();
    }

    private void OnDrawGizmosSelected()
    {
        if(drawDebugPoints && procMesh.vertexCount < 5000)
        {
            Gizmos.color = Color.green;
            foreach(var point in procMesh.vertices)
            {
                Gizmos.DrawSphere(point + transform.position, 0.025f);
            }
        }

        if(drawDebugNormals && procMesh.vertexCount < 5000)
        {
            Gizmos.color = Color.blue;
            for(int i = 0; i < procMesh.vertexCount; ++i)
            {
                var point = procMesh.vertices[i] + transform.position;
                Gizmos.DrawLine(point, point + procMesh.normals[i] * 0.1f);
            }
        }
    }

    private void Awake()
    {
        RegenerateMesh();
        VoxelController.OnVoxelsChangedEvent += RegenerateMesh;
    }

    public void ForceRegenMesh() { RegenerateMesh(); }

    private void RegenerateMesh()
    {
        int numVoxels = voxelController.GetVoxels().Length;
        
        var vertices = new List<Vector3>();
        var normals = new List<Vector3>();
        var uvs = new List<Vector2>();
        var triangles = new List<int>();

        int numQuads = 0;

        for(int i = 0; i < numVoxels; ++i)
        {
            if(voxelController.IsVoxelEmpty(i)) continue;

            var voxelPosition = voxelController.GetVoxelRelativePosition(i);

            // Generate six faces for our cube
            for(byte j = 0; j < 6; ++j)
            {
                var adjacentVoxelIndex = voxelController.GetAdjacentVoxel(i, j);
                if(adjacentVoxelIndex != null)
                {
                    // Skip this face if there's a valid voxel in that direction
                    if(voxelController.IsVoxelNotEmpty(adjacentVoxelIndex)) continue;
                }

                var face = ProceduralMeshHelper.MakeCubeFace(j, VoxelController.VoxelSize, voxelPosition.Value);
                foreach(var vertex in face.Vertices)
                {
                    vertices.Add(vertex.Position);
                    normals.Add(vertex.Normal);
                    uvs.Add(vertex.Uvs);
                }

                // Shift the indices by 4 for every quad added before this one
                int indexOffset = numQuads * 4;
                foreach(var index in face.Indices)
                {
                    triangles.Add(index + indexOffset);
                }
                ++numQuads;
            }
        }

        procMesh.Clear();

        procMesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        procMesh.SetVertices(vertices);
        procMesh.SetNormals(normals);
        procMesh.SetUVs(0, uvs);
        procMesh.SetTriangles(triangles, 0);

        numVertices = vertices.Count;
        numIndices = triangles.Count;
    }

    // TODO: Build mesh data based on each 2D axis pair (xy, xz, yz) so we can combine quads to save geo


    private void GenerateMeshForAxis()
    {
    }

    private void GenerateMeshForPlane(int planeDepth)
    {

    }
}
