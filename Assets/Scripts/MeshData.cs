﻿using UnityEngine;


public static class MeshData
{

    public static readonly Vector3[] CubePoints =
        { // Unity coordinate system: X = right, Y = up, Z = forward
            new Vector3(-0.5f,  0.5f,  0.5f), // 0 Front top left
            new Vector3( 0.5f,  0.5f,  0.5f), // 1 Front top right
            new Vector3(-0.5f, -0.5f,  0.5f), // 2 Front bottom left
            new Vector3( 0.5f, -0.5f,  0.5f), // 3 Front bottom right
            new Vector3(-0.5f,  0.5f, -0.5f), // 4 Back top left
            new Vector3( 0.5f,  0.5f, -0.5f), // 5 Back top right
            new Vector3(-0.5f, -0.5f, -0.5f), // 6 Back bottom left
            new Vector3( 0.5f, -0.5f, -0.5f)  // 7 Back bottom right 
        };

    public static readonly Vector3[] CubeNormals =
        {
            new Vector3( 0.0f,  0.0f,  1.0f), // 0 Front
            new Vector3( 0.0f,  0.0f, -1.0f), // 1 Back
            new Vector3( 1.0f,  0.0f,  0.0f), // 2 Right
            new Vector3(-1.0f,  0.0f,  0.0f), // 3 Left
            new Vector3( 0.0f,  1.0f,  0.0f), // 4 Top
            new Vector3( 0.0f, -1.0f,  0.0f)  // 5 Bottom
        };

    public static readonly Vector2[] QuadUVs =
        {
            new Vector2( 0.0f,  1.0f), // 0 Top left
            new Vector2( 1.0f,  1.0f), // 1 Top right
            new Vector2( 0.0f,  0.0f), // 2 Bottom left
            new Vector2( 1.0f,  0.0f), // 3 Bottom right
        };

    public static readonly int[][] CubeQuadPoints =
        {
            new[] { 1, 0, 3, 2 }, // 0 Front
            new[] { 4, 5, 6, 7 }, // 1 Back
            new[] { 5, 7, 1, 3 }, // 2 Right
            new[] { 0, 2, 4, 6 }, // 3 Left
            new[] { 0, 1, 4, 5 }, // 4 Top
            new[] { 6, 7, 2, 3 }  // 5 Bottom
        };

    public static readonly int[][] CubeQuadUVs =
        {
            new[] { 0, 1, 2, 3 }, // 0 Front
            new[] { 0, 1, 2, 3 }, // 1 Back
            new[] { 0, 2, 1, 3 }, // 2 Right
            new[] { 0, 2, 1, 3 }, // 3 Left
            new[] { 0, 1, 2, 3 }, // 4 Top
            new[] { 3, 2, 1, 0 }  // 5 Bottom
        };

    public static readonly int[][] QuadIndices =
        {
            // Clockwise
            new[] { 0, 2, 1,    // First tri
                        1, 2, 3 },  // Second tri
            // Anti-clockwise
            new[] { 1, 2, 0,    // First tri
                        3, 2, 1 }   // Second tri
        }; 

}
